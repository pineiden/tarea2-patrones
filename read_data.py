import tifffile as tiff
from data import *
import pickle
from pathlib import Path
# Importamos todos los paquetes y módulos para entrenar el modelo
from PIL import Image

import numpy as np
import pandas as pd


import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator


def read():
    raw_path = Path("./datos_tarea2") / "raw.tif"
    referencia_path = Path("./datos_tarea2") / "segmentacion_referencia.tif"
    # for img in imageio.imread(path):
    #    print(img.shape)
    raw_images = tiff.imread(raw_path)
    referencia_images = tiff.imread(referencia_path)
    return {"data": raw_images, "labels": referencia_images}

from tensorflow import keras
import tifffile as tiff
from data import *
import pickle
from pathlib import Path
# Importamos todos los paquetes y módulos para entrenar el modelo
from PIL import Image

import numpy as np
import pandas as pd


import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator


raw_path = Path("./datos_tarea2") / "raw.tif"
referencia_path = Path("./datos_tarea2") / "segmentacion_referencia.tif"


# for img in imageio.imread(path):
#    print(img.shape)
raw_images = tiff.imread(raw_path)
referencia_images = tiff.imread(referencia_path)


# Carga tus datos de imágenes y etiquetas aquí
# Supongamos que tus imágenes están almacenadas en un arreglo llamado 'images'
# y las etiquetas correspondientes en un arreglo llamado 'labels'

# Crea un generador de datos de imágenes para realizar la data augmentation
datagen = ImageDataGenerator(
    # Aquí puedes configurar los diferentes parámetros de data augmentation
    rotation_range=20,  # Rango de rotación aleatoria en grados
    width_shift_range=0.2,  # Rango de desplazamiento horizontal aleatorio
    height_shift_range=0.2,  # Rango de desplazamiento vertical aleatorio
    zoom_range=0.2,  # Rango de zoom aleatorio
    horizontal_flip=True,  # Volteo horizontal aleatorio
    vertical_flip=True  # Volteo vertical aleatorio
)

# Genera las imágenes aumentadas y sus etiquetas correspondientes
augmented_images = []
augmented_labels = []

for image, label in zip(raw_images, referencia_images):
    print(image.shape, label.shape)

    # Expande las dimensiones de la imagen para que tenga forma (1, 512, 512, 1)
    image = tf.expand_dims(image, axis=0)
    label = tf.expand_dims(label, axis=0)
    image = tf.expand_dims(image, axis=-1)
    label = tf.expand_dims(label, axis=-1)
    print(image.shape, label.shape)
    # Genera las imágenes aumentadas y sus etiquetas
    augmented_images.extend(datagen.flow(image, batch_size=1))
    augmented_labels.extend(datagen.flow(label, batch_size=1))

# Convierte las listas a arreglos numpy
augmented_images = tf.concat(augmented_images, axis=0)
augmented_labels = tf.concat(augmented_labels, axis=0)


print("Data augmented")
# Verifica las formas de los arreglos resultantes
print(augmented_images.shape)  # (N, 512, 512, 1)
print(augmented_labels.shape)  # (N, 512, 512, 1)


def imgs_mean(image):
    ''' Toma un arreglo de imagenes y calcula la intensidad a cada pixel por imagen, retorna  el arreglo transformado '''

    # Convertir la imagen a tipo float en el rango [0, 1]
    image_float = image.astype(np.float32) / 255.0

    # Crear un kernel de promedio
    kernel_size = (3, 3)
    kernel = np.ones(kernel_size) / np.prod(kernel_size)

    # Crear una lista para almacenar los resultados
    result_images = []

    # Aplicar la convolución a cada capa de la imagen
    for i in range(image.shape[0]):
        # Obtener la capa actual
        layer = image_float[i, ...]

        # Expandir las dimensiones del kernel para que coincida con el número de canales de la capa
        expanded_kernel = np.expand_dims(
            np.expand_dims(kernel, axis=-1), axis=-1)

        # Convertir la capa en un tensor 4D con forma (1, height, width, 1)
        layer_tensor = tf.expand_dims(tf.expand_dims(layer, axis=0), axis=-1)

        # Aplicar la convolución con keras.Conv2D
        conv_layer = keras.layers.Conv2D(
            filters=1, kernel_size=kernel_size, padding='same')(layer_tensor)

        # Obtener la imagen resultante de la convolución y eliminar las dimensiones adicionales
        result_image = tf.squeeze(conv_layer).numpy()

        # Agregar la imagen resultante a la lista
        result_images.append(result_image)

    # Convertir la lista de imágenes en un arreglo numpy
    result_images = np.array(result_images)
    return result_images


def imgs_laplacian(images):
    ''' Toma un arreglo de imagenes y calcula el laplaciano a cada pixel por imagen, retorna  el arreglo transformado '''
    # Convertir la imagen a tipo float en el rango [0, 1]
    image_float = images.astype(np.float32) / 255.0

    # Crear el kernel del laplaciano
    kernel = np.array([[0, 1, 0], [1, -4, 1], [0, 1, 0]])

    # Crear una lista para almacenar los resultados
    result_images = []

    # Aplicar la convolución con el kernel del laplaciano a cada capa de la imagen
    for i in range(images.shape[0]):
        # Obtener la capa actual
        layer = image_float[i, :, :]

        # Expandir las dimensiones del kernel para que coincida con el número de canales de la capa
        expanded_kernel = np.expand_dims(
            np.expand_dims(kernel, axis=-1), axis=-1)

        # Convertir la capa en un tensor 4D con forma (1, height, width, 1)
        layer_tensor = tf.expand_dims(tf.expand_dims(layer, axis=0), axis=-1)

        # Aplicar la convolución con keras.Conv2D y el kernel del laplaciano
        conv_layer = keras.layers.Conv2D(filters=1, kernel_size=3, padding='same', use_bias=False,
                                         trainable=False, weights=[expanded_kernel])(layer_tensor)

        # Obtener la imagen resultante de la convolución y eliminar las dimensiones adicionales
        result_image = tf.squeeze(conv_layer).numpy()

        # Redimensionar la imagen a un tamaño más grande
        # Inspeccion:esta bien esto??? al comentarlo  obtengo imgs 512x512 (tamaño original)

        # result_image_resized = result_image.resize((500, 500))

        # Agregar la imagen resultante a la lista
        result_images.append(result_image)

    # entregamos np.array para ser consistentes
    result_images = np.array(result_images)
    return result_images


def grad_calculate(images):
    ''' Toma un arreglo de imagenes y calcula la magnitud del gradiente asociado a cada pixel y sus  dimensiones por cordenada para  cada imagen, retorna  el arreglo transformado '''

    # Convertir la imagen a tipo float en el rango [0, 1]
    image_float = images.astype(np.float32) / 255.0

    # Crear el kernel de Sobel en la dirección x
    kernel_x = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]])

    # Crear el kernel de Sobel en la dirección y
    kernel_y = np.array([[-1, -2, -1], [0, 0, 0], [1, 2, 1]])

    # Crear una lista para almacenar los resultados del gradiente en x
    grad_x_images = []

    # Crear una lista para almacenar los resultados del gradiente en y
    grad_y_images = []

    # lista para las magnitudes
    grad_magnitudes = []

    # Aplicar la convolución con el kernel de Sobel a cada capa de la imagen
    for i in range(images.shape[0]):
        # Obtener la capa actual
        layer = image_float[i, :, :]

        # Expandir las dimensiones de los kernels para que coincidan con el número de canales de la capa
        expanded_kernel_x = np.expand_dims(
            np.expand_dims(kernel_x, axis=-1), axis=-1)
        expanded_kernel_y = np.expand_dims(
            np.expand_dims(kernel_y, axis=-1), axis=-1)

        # Convertir la capa en un tensor 4D con forma (1, height, width, 1)
        layer_tensor = tf.expand_dims(tf.expand_dims(layer, axis=0), axis=-1)

        # Aplicar la convolución con keras.Conv2D y el kernel de Sobel en la dirección x
        conv_x_layer = keras.layers.Conv2D(filters=1, kernel_size=3, padding='same', use_bias=False,
                                           trainable=False, weights=[expanded_kernel_x])(layer_tensor)

        # Aplicar la convolución con keras.Conv2D y el kernel de Sobel en la dirección y
        conv_y_layer = keras.layers.Conv2D(filters=1, kernel_size=3, padding='same', use_bias=False,
                                           trainable=False, weights=[expanded_kernel_y])(layer_tensor)

        # Obtener las imágenes resultantes de los gradientes en x e y y eliminar las dimensiones adicionales
        grad_x_image = tf.squeeze(conv_x_layer).numpy()
        grad_y_image = tf.squeeze(conv_y_layer).numpy()

        # Calcular la magnitud del gradiente
        grad_magnitude = np.sqrt(grad_x_image**2 + grad_y_image**2)

        # Reescalar los valores a escala de grises de 0 a 255
        grad_magnitude_rescaled = (grad_magnitude - np.min(grad_magnitude)) * \
            255 / (np.max(grad_magnitude) - np.min(grad_magnitude))

        # Convertir a tipo entero
        grad_magnitude_rescaled = grad_magnitude_rescaled.astype(np.uint8)

        grad_magnitudes.append(grad_magnitude_rescaled)
        # Redimensionar la imagen a un tamaño más grande
        # grad_magnitude_resized = Image.fromarray(grad_magnitude_rescaled).resize((500, 500))

        # Agregar las imágenes resultantes a las listas
        grad_x_images.append(grad_x_image)
        grad_y_images.append(grad_y_image)
    # entregamos np.array para ser consistentes
    grad_magnitudes = np.array(grad_magnitudes)
    return grad_magnitudes, grad_x_images, grad_y_images


def imgs_gradient(images):
    ''' Toma un arreglo de imagenes y calcula la magnitud del gradiente asociado a cada pixel para  cada imagen, retorna  el arreglo transformado '''
    grad_magnitudes, grad_x, grad_y = grad_calculate(images)
    return grad_magnitudes


print("Operando convoluciones")

images = augmented_images
means = imgs_mean(images)
laplacians = imgs_laplacian(images)
gradients = imgs_gradient(images)

X_charac = np.stack((augmented_images, laplacians, means, gradients), axis=3)


images_data = {
    "data": X_charac,
    "labels": augmented_labels
}

databytes = pickle.dumps(images_data)
path = Path("./data-aug.data")
path.write_bytes(databytes)
